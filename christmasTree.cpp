#include "christmasTree.h"
using namespace std;

char Tree::foreground = '#';
char Tree::background = '.';

  ostream& operator<< (ostream& out, const Tree& tree ) {
    cout << "Tree Name: " << tree.mName << endl; 
    cout << "Tree Nickname: " ; 
    if ( tree.mNickname  ) {
      cout << tree.mNickname ; 
    }
    cout << endl; 
    // TODO: fill in the missing codes;
    cout << endl;
    string t (tree.mLeftMargin, ' ');
    for (auto& s:tree.mRows) {
      cout << t+ s << endl;
    }


    return out;
  }

  // normal cstr
  Tree::Tree(int n, const string& name, size_t leftMargin ) : mName(name), mLeftMargin(leftMargin)  { 
    layer(n); 
    mNickname = new char (name.size() + 1);
    // strcpy( mNickname, name.c_str());
    // strcpy_s ( mNickname, name.size(), name.c_str());
  };

  // TODO: normal copy cstr
  // TODO: normal assignment 
  // TODO: move copy cstr
  // TODO: move assignment 


bool Tree::operator< ( const Tree& o ) const {
  return height() < o.height();
}
bool Tree::operator> ( const Tree& o ) const {
  return height() > o.height();
}

void Tree::rectangle(size_t height, size_t width, size_t offset ) {
  // TODO: fill in the missing codes;
}
// TODO: fill in the missing codes;
void Tree::trapzoid(size_t start, size_t n, size_t offset ) {
    // TODO: fill in the missing codes;
}
void Tree::layer(size_t n ) {
  int offset = 20; //  <------------
  for (size_t i=1; i < n; i=i+2 ) {
    trapzoid( i/2, i, offset ); // <-------------
  }
  size_t w = n/3;
  if ( w%2==0) ++w;
  rectangle(n/2, w, offset ) ; // <-------------
}
void Tree::augument( size_t n ) {
  // if (height() == 0 ) return ;

  size_t w = width();
  size_t start = height();
  for (size_t i=start; i < n; ++i ) {
    string  s (w, background) ;
    mRows.push_front ( move ( s ) ) ;
  }
}

void Tree::operator+= ( Tree&& o ) {
  size_t n1 = height();
  size_t n2 = o.height();
  size_t n  = max (n1,n2);
  augument(n);
  o.augument(n);

  size_t i = 0 ;
  mName = move ( o.mName );
  
  free ( mNickname );
  mNickname = o.mNickname ;
  o.mNickname = nullptr ;

  for ( auto&s: mRows ) {
    s = s + move ( o.mRows[i++] );
  }
  o.clear ();
}

