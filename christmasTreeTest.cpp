#include "christmasTree.h"
using namespace std;

void unitTest1()
{
  cout << __func__ << endl;
  vector<Tree> v;
  size_t n = 5;
  for (size_t i = 0 ; i < n; ++i ) {
    Tree tree ( rand()%10+6, "tree_" + to_string(i));
    v.push_back ( move (tree) );
  }
  sort (v.begin(), v.end() );
  sort (v.begin(), v.end(), greater<Tree>() );
  sort (v.begin(), v.end(), less<Tree>() );

  for (size_t i = 1 ; i < n; ++i ) {
    v[0]+= ( move (v[i]) );
  }
  for ( Tree& t:v ) {
    cout << t << endl;
  }

}
void unitTest2 () {
  cout << __func__ << endl;
  Tree x(0, "x");
  Tree y(10, "y");
  x+=( move(y) ) ;
  cout << x << endl;
  cout << y << endl;
}
void unitTest3 () {

  cout << __func__ << endl;
  
  Tree x(0, "figTree", 30 );
  Tree y(10, "xTree", 30 );

  x = move(y) ;
  cout << x << endl;
  cout << y << endl;

} 

int main ()
{
  string fn = "output.txt";

  // freopen( fn.c_str(),"w",stdout);
  unitTest1();
  //unitTest2();
  unitTest3();

  // getchar();
  return 0;
}


