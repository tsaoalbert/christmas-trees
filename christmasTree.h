
#pragma once
#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>
#include <string>
#include <functional>

using namespace std;

struct Tree 
{
  friend ostream& operator<< (ostream& out, const Tree& tree ) ;

  public:
    // dstr
    virtual ~Tree () {
      if ( mNickname!=nullptr ) {
        //delete [] mNickname;
        //mNickname= nullptr ;
      }
    }

    // cstr
    Tree(int n, const string& name="", size_t leftMargin = 0) ;

    // Tree ( const Tree& o ) ; // normal copy cstr
    // Tree& operator= ( const Tree& o ) ; // normal assignment 
    // Tree ( Tree&& o ) noexcept  ; // move copy cstr
    // Tree& operator= ( Tree&& o ) noexcept  ; // move assignment 

    void operator+= ( Tree&& o ) ;
    inline size_t height() const { return mRows.size(); } ;
    inline size_t width() const { return (height()==0)?0: mRows[0].size(); } ;
    inline void clear() { mRows.clear(); } ;
    bool operator< ( const Tree& o ) const ;
    bool operator> ( const Tree& o ) const ;


  private:
    void rectangle(size_t height, size_t width, size_t offset ) ;
    void trapzoid(size_t start, size_t n, size_t offset) ;
    void layer(size_t n) ;
    void augument (size_t n) ;
  
  private:
    string mName;
    char*  mNickname;
    size_t mLeftMargin = 0 ;
    deque< string > mRows;
    static char background ;
    static char foreground ;
};

